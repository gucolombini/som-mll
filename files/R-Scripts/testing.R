setwd("../Datasets/iterative/")
library("kohonen")
library("RWeka")

# Functions
# Multi-label precision, recall, f-measure
# ------------------------------------------------------------
evaluation <- function(predictions,true.classes,threshold){

    sum.prec <- 0
    sum.reca <- 0
    sum.fm <- 0
    sum.acc <- 0

    for(i in 1:nrow(predictions)){
        bin.pred <- (predictions[i,] > threshold) * 1
        true <- true.classes[i,]

        # See where both positions are 1
        intersection <- length(which( (bin.pred==1) & (true==1) ))

        # True and predicted
        predicted <- length(which( (bin.pred==1) ))
        true.c <- length(which( (true==1) ))

        # Sum true and predicted
        sum.true.pred <- predicted + true.c

        # Union true and predicted
        union.true.pred <- union(which( (bin.pred==1) ),which( (true==1) ))
        union.true.pred <- length(union.true.pred)

        # Sums
        if(predicted > 0){
            sum.prec <- sum.prec + (intersection / predicted)
        }
        if(true.c > 0){
			sum.reca <- sum.reca + (intersection / true.c)
		}
		if(sum.true.pred > 0){
			sum.fm <- sum.fm + ((2 * intersection) / sum.true.pred)
        }
		if(union.true.pred > 0){
			sum.acc <- sum.acc + (intersection / union.true.pred)
		}
    }

    # Total values
    total.prec <- sum.prec / nrow(predictions)
    total.reca <- sum.reca / nrow(predictions)
    total.fm <- sum.fm / nrow(predictions)
    total.acc <- sum.acc / nrow(predictions)

	cat(total.prec,"\n")
	cat(total.reca,"\n")
	cat(total.fm,"\n")
	cat(total.acc,"\n\n")

    return(c(total.prec,total.reca,total.fm,total.acc))
}


# read datasets test
dataset1 <- read.arff("MultiLabelData_test_fold_1.arff")
dataset2 <- read.arff("MultiLabelData_test_fold_2.arff")
dataset3 <- read.arff("MultiLabelData_test_fold_3.arff")
dataset4 <- read.arff("MultiLabelData_test_fold_4.arff")
dataset5 <- read.arff("MultiLabelData_test_fold_5.arff")
dataset6 <- read.arff("MultiLabelData_test_fold_6.arff")
dataset7 <- read.arff("MultiLabelData_test_fold_7.arff")
dataset8 <- read.arff("MultiLabelData_test_fold_8.arff")
dataset9 <- read.arff("MultiLabelData_test_fold_9.arff")
dataset10 <- read.arff("MultiLabelData_test_fold_10.arff")

# read datasets train
datasetTreino1 <- read.arff("MultiLabelData_train_fold_1.arff")
datasetTreino2 <- read.arff("MultiLabelData_train_fold_2.arff")
datasetTreino3 <- read.arff("MultiLabelData_train_fold_3.arff")
datasetTreino4 <- read.arff("MultiLabelData_train_fold_4.arff")
datasetTreino5 <- read.arff("MultiLabelData_train_fold_5.arff")
datasetTreino6 <- read.arff("MultiLabelData_train_fold_6.arff")
datasetTreino7 <- read.arff("MultiLabelData_train_fold_7.arff")
datasetTreino8 <- read.arff("MultiLabelData_train_fold_8.arff")
datasetTreino9 <- read.arff("MultiLabelData_train_fold_9.arff")
datasetTreino10 <- read.arff("MultiLabelData_train_fold_10.arff")

# creating the testing list with the datasets
listTeste <- list(dataset1, dataset2, dataset3, dataset4, dataset5, dataset6, dataset7, dataset8, dataset9, dataset10)

# creating the training list with the datasets 
listTreino <- list(datasetTreino1,datasetTreino2, datasetTreino3, datasetTreino4, datasetTreino5, 
                   datasetTreino6, datasetTreino7, datasetTreino8, datasetTreino9, datasetTreino10)

# after read
# precisionGeral = 0
# fmeasureGeral = 0
# recallGeral = 0

precision <- vector()
recall <- vector()
fmeasure <- vector()
accuracy <- vector()

for (h in 1:length(listTreino)) {
 
	# train
	dataset <- listTreino[[h]]
  
	# declaring a few helpful values
	class.number = length(grep("Class", colnames(dataset),value=F)[1]:ncol(dataset))
	begin.class = grep("Class",colnames(dataset),value=F)[1]
	end.class = ncol(dataset)
	attr.number = grep("Class",colnames(dataset),value=F)[1]-1 
  
	# scaling
	dataset[,1:attr.number] <- scale(dataset[,1:attr.number])
	dataset.sc.matrix <- as.matrix(dataset[,1:attr.number]) 

	# creating the self organized map
	som.grid <- somgrid(xdim=5, ydim=5, topo="hexagonal")
	data.som <- som(dataset.sc.matrix, grid=som.grid)

	# test
	test <- listTeste[[h]]
	test.sc.matrix <- data.frame(matrix(data = 0, nrow=nrow(test), ncol=ncol(test)))

	# scaling
	test.sc.matrix[,1:attr.number] <- scale(test[,1:attr.number])
	test.sc.matrix[,begin.class:end.class] <- test[,begin.class:end.class]
	colnames(test.sc.matrix) <- colnames(test)
  
	# =========== #
	vetor <- c()
	vetor2 <- c()
 
    browser()  
	# using test
	for (i in 1:nrow(test.sc.matrix)) {
		for (j in 1:nrow(data.som$codes)) {
    		# seeks the shorter distance between each example line related to the som
    		x <- rbind(test.sc.matrix[i,], data.som$codes[j,])
    		vetor <- c(vetor, dist(x, method="euclidean"))
        }
		vetor2 <- c(vetor2, which(vetor==min(vetor)))
		vetor <- c()
	}
	# final output is vetor2
	# generic matrixArmazena - will be used to store the means
	matrixArmazena <- matrix(nrow=nrow(test.sc.matrix), ncol = class.number)

	for (i in 1:nrow(test.sc.matrix)) {
    	vetVar <- c(which(data.som$unit.classif == vetor2[i]))
    	# mean
    	if (length(vetVar) > 1) {
      		matrixAux <- dataset[vetVar, begin.class:end.class]
     		# if vetVar > 1 makes mean
    	 	matrixAux <- apply(matrixAux, 2, as.numeric)
    		matrixAux <- apply(matrixAux, 2, mean)
     		matrixArmazena[i,] <- matrixAux
		} else if (length(vetVar) == 1) {
    		matrixAux <- dataset[vetVar, begin.class:end.class]  		
    		# if vetVar = 1 simply store its own value
			matrixAux <- apply(matrixAux, 2, as.numeric)
			# storage
   			matrixArmazena[i,] <- matrixAux
   		}
		matrixAux <- matrix()
	}
	# calculate precision and recall  
	tp = 0
	tn = 0
	fp = 0
	fn = 0
	threshold <- 0.5

	true.classes <- test[,grep("Class",colnames(test))]
	precision.recall.fm.acc <- evaluation(matrixArmazena,true.classes,threshold)

	precision <- c(precision,precision.recall.fm.acc[1])
	recall <- c(recall,precision.recall.fm.acc[2])
	fmeasure <- c(fmeasure,precision.recall.fm.acc[3])
	accuracy <- c(accuracy,precision.recall.fm.acc[4])
}

cat("\nMean Precision = ",mean(precision)," (",sd(precision),") , Mean Recall = ",mean(recall)," (",sd(recall), ")",sep="")
cat("\nMean F-measure = ",mean(fmeasure)," (",sd(fmeasure),") , Mean Accuracy = ",mean(accuracy)," (",sd(accuracy), ")",sep="")
